<div class="coupons">
		<div class="coupons-grids text-center">
			<div class="w3layouts_mail_grid">
			<?php 
			    $grids = $link->prepare("SELECT * FROM `short_description`");
				$grids->execute();
				$result = $grids->get_result();
				while($data = $result->fetch_assoc()){
			?>
				<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<?php if($data['id'] == 1){
							    echo "<i class=\"fa fa-truck\" aria-hidden=\"true\"></i>";
						}elseif($data['id'] == 2){
							    echo "<i class=\"fa fa-headphones\" aria-hidden=\"true\"></i>";
						}elseif($data['id'] == 3){
							    echo "<i class=\"fa fa-shopping-bag\" aria-hidden=\"true\"></i>";
						}else{
							echo "<i class=\"fa fa-gift\" aria-hidden=\"true\"></i>";
						}
						  ?>
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3><?php echo $data['head']; ?></h3>
						<p><?php echo $data['txt']; ?></p>
					</div>
				</div>
			<?php 
				}
			?>
				<div class="clearfix"> </div>
			</div>

		</div>
</div>