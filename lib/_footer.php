<footer>
<div class="footer">
	<div class="footer_agile_inner_info_w3l">
		<div class="col-md-3 footer-left">
			<h2><a href="index.html"><span>M</span>Mobile </a></h2>
			<p>Lorem ipsum quia dolor
			sit amet, consectetur, adipisci velit, sed quia non 
			numquam eius modi tempora.</p>
			<ul class="social-nav model-3d-0 footer-social w3_agile_social two">
														
                <?php 
                    $shair = $link->query("SELECT `txt` FROM `contact` WHERE `cat` = 'Share' LIMIT 0, 1");
                      $row = @mysqli_fetch_assoc($shair);
                              ?>
				<li><a href="<?php echo  $row['txt']; ?>" class="facebook">
					  <div class="front"><i class="fa fa-facebook" aria-hidden="true"></i></div>
					  <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div></a></li>
			  <?php 
                              $shair = $link->query("SELECT `txt` FROM `contact` WHERE `cat` = 'Share' LIMIT 1, 1");
                              $row = @mysqli_fetch_assoc($shair);
                              ?>
				<li><a href="<?php echo  $row['txt']; ?>" class="twitter"> 
					  <div class="front"><i class="fa fa-twitter" aria-hidden="true"></i></div>
					  <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i></div></a></li>
			  <?php 
                              $shair = $link->query("SELECT `txt` FROM `contact` WHERE `cat` = 'Share' LIMIT 2, 1");
                              $row = @mysqli_fetch_assoc($shair);
                              ?>
				<li><a href="<?php echo $row['txt']; ?>" class="instagram">
					  <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i></div>
					  <div class="back"><i class="fa fa-instagram" aria-hidden="true"></i></div></a></li>
			  <?php 
                              $shair = $link->query("SELECT `txt` FROM `contact` WHERE `cat` = 'Share' LIMIT 3, 1");
                              $row = @mysqli_fetch_assoc($shair);
                              ?>
				<li><a href="<?php echo $row['txt']; @mysqli_free_result($shair); ?>" class="pinterest">
					  <div class="front"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
					  <div class="back"><i class="fa fa-linkedin" aria-hidden="true"></i></div></a></li>
			</ul>
		</div>
		<div class="col-md-9 footer-right">
			<div class="sign-grds">
				<div class="col-md-4 sign-gd">
					<h4>Our <span>Information</span> </h4>
					<ul>
						<li><a href="index">Home</a></li>
						
						<li><a href="about">About</a></li>
						
						<li><a href="contact">Contact</a></li>
					</ul>
				</div>
				
				<div class="col-md-5 sign-gd-two">
					<h4>Store <span>Information</span></h4>
					<div class="w3-address">
						<div class="w3-address-grid">
							<div class="w3-address-left">
								<i class="fa fa-phone" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Phone Number</h6>
								<?php 
									$foot = $link->query("SELECT `txt` FROM `contact` WHERE `cat` = 'Footer' LIMIT 0, 1");
									$rows = @mysqli_fetch_assoc($foot);
								?>
								<p><?php echo $rows['txt']; ?></p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="w3-address-grid">
							<div class="w3-address-left">
								<i class="fa fa-envelope" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Email Address</h6>
								<?php 
									$foot = $link->query("SELECT `txt` FROM `contact` WHERE `cat` = 'Footer' LIMIT 1, 1");
									$rows = @mysqli_fetch_assoc($foot);
								?>
								<p>Email :<a href="mailto:<?php echo $rows['txt']; ?>"><?php echo $rows['txt']; ?></a></p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="w3-address-grid">
							<div class="w3-address-left">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
							</div>
							<div class="w3-address-right">
								<h6>Location</h6>
								<?php 
									$foot = $link->query("SELECT `txt` FROM `contact` WHERE `cat` = 'Footer' LIMIT 2, 1");
									$rows = @mysqli_fetch_assoc($foot);
								?>
								<p><?php echo $rows['txt']; @mysqli_free_result($foot); ?></p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
				<div class="col-md-3 sign-gd flickr-post">
					<h4>Our <span>Video</span></h4>
					<ul>
						<li><a href="#"><img src="images/t1.jpg" alt=" " class="img-responsive" /></a></li>
						<li><a href="#"><img src="images/t2.jpg" alt=" " class="img-responsive" /></a></li>
						<li><a href="#"><img src="images/t3.jpg" alt=" " class="img-responsive" /></a></li>
						<li><a href="#"><img src="images/t4.jpg" alt=" " class="img-responsive" /></a></li>
						<li><a href="#"><img src="images/t1.jpg" alt=" " class="img-responsive" /></a></li>
						<li><a href="#"><img src="images/t2.jpg" alt=" " class="img-responsive" /></a></li>
						<li><a href="#"><img src="images/t3.jpg" alt=" " class="img-responsive" /></a></li>
						<li><a href="#"><img src="images/t2.jpg" alt=" " class="img-responsive" /></a></li>
						<li><a href="#"><img src="images/t4.jpg" alt=" " class="img-responsive" /></a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
			
		<p class="copy-right">&copy; <?php echo date('Y'); ?> Mallick Mobile. All rights reserved | Design by <span style="color:#2fdab8; font-weight:600;">CREATIVE WEB</span></p>
	</div>
</div>
</footer>
<!-- //footer -->