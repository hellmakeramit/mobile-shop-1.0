<?php
/**
 * Created by PhpStorm.
 * User: amit
 * Date: 7/20/2018
 * Time: 12:35 AM
 */
?>

<!-- /new_arrivals -->
<div class="new_arrivals_agile_w3ls_info">
    <div class="container">
        <h3 class="wthree_text_info">New <span>Arrivals</span></h3>
        <div id="horizontalTab">
            <ul class="resp-tabs-list">
                <li> Mobile</li>
                <li> Headphone</li>
                <li> Battery</li>
                <li> Other Accessories</li>
            </ul>
            <div class="resp-tabs-container">
                
                <div class="tab1">
                    <?php 

                    $sql_headphone="SELECT * FROM `all_other_item` WHERE `new_id`!=0  AND (`category` LIKE '%Head phone%'
                or `category` LIKE '%Head Phone%' or `category` LIKE '%head phone%' or `category` LIKE '%HeadPhone%'
                or `category` LIKE '%headphone%' or `category` LIKE '%Head_Phone%' or `category` LIKE '%head_phone%'
                    )
                    ";
                    $queryheadphone=$link->query($sql_headphone);
                    $rowheadphone=mysqli_fetch_all($queryheadphone,MYSQLI_ASSOC);
                   
                    $sql_battery="SELECT * FROM `all_other_item` WHERE `new_id`!=0  AND (`category` LIKE '%Battery%'
                or `category` LIKE '%battery%' or `category` LIKE '%bateryhead phone%' or `category` LIKE '%bateri%'
                or `category` LIKE '%batt%' or `category` LIKE '%Batt%' or `category` LIKE '%Batteri%'
                    )
                    ";
                    $querybattery=$link->query($sql_battery);
                    $rowbattery=mysqli_fetch_all($querybattery,MYSQLI_ASSOC);


                   $sql_accessories="SELECT * FROM `all_other_item` WHERE `new_id`!=0  AND (`category` NOT LIKE '%Battery%'
                AND `category` NOT LIKE '%battery%' AND `category` NOT LIKE '%bateryhead phone%' AND `category` NOT LIKE '%bateri%'
                AND `category` NOT LIKE '%batt%' AND `category` NOT LIKE '%Batt%' AND `category` NOT LIKE '%Batteri%'
                    )   AND (`category` NOT LIKE '%Head phone%'
                AND `category` NOT LIKE '%Head Phone%' AND `category` NOT LIKE '%head phone%' AND `category` NOT LIKE '%HEADPHONE%'
                AND `category` NOT LIKE '%headphone%' AND `category` NOT LIKE '%HEAD PHONE%' AND `category` NOT LIKE '%HEAD_PHONE%'
                AND `category` NOT LIKE '%head_phone%'
                    )
                    ";
                    $queryaccessories=$link->query($sql_accessories);
                    $rowaccessories=mysqli_fetch_all($queryaccessories,MYSQLI_ASSOC);


                    $sqlmobile="SELECT * FROM `all_mobile_item` WHERE `new_id`=1";
                    $querymobile=$link->query($sqlmobile);
                    $rowmobile=mysqli_fetch_all($querymobile,MYSQLI_ASSOC);
                    foreach( $rowmobile as $valmob){
                        ?>

                             <div class="col-md-3 product-men">
                        <div class="men-pro-item simpleCart_shelfItem">
                            <div class="men-thumb-item" id="newidallimg">
                                <img src="<?php echo $valmob['img1']; ?>" alt="" class="pro-image-front" style="height: 350px">
                                <img src="<?php echo $valmob['img2']; ?>" alt="" class="pro-image-back" style="height: 350px">
                                <div class="men-cart-pro">
                                    <div class="inner-men-cart-pro">
                                        <a href="#" class="link-product-add-cart" id="<?php echo $valmob['id']?>">Quick View</a>
                                    </div>
                                </div>
                                <span class="product-new-top">New</span>

                            </div>
                            <div class="item-info-product ">
                                <h4 style="color:#f00; font-size:15px;"><?php echo $valmob['name']; ?></h4>
                                <div class="info-product-price">
                                    <?php

                                    ?>
                                    <span class="item_price"><?php echo $valmob['o_price']!=''? $valmob['o_price']."/- Rs":$valmob['c_price']."/- Rs" ?></span>
                                    <del><?php echo $valmob['o_price']!=''? $valmob['c_price']."/- Rs":false?></del>
                                </div>
                                <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
                                    <form action="#" method="post">
                                        <fieldset>
                                            <input type="hidden" name="cmd" value="_cart" />
                                            <input type="hidden" name="add" value="1" />

                                            <input type="hidden" name="business" value="<?php echo $valmob['id']?>" />
                                            <input type="hidden" name="item_name" value="<?php echo $valmob['name']?>" />
                                            <input type="hidden" name="amount" value="<?php echo $valmob['o_price']==''? $valmob['c_price']:$valmob['o_price']?>" />
                                            <input type="hidden" name="discount_amount" value="<?php //echo $valmob['c_price']-$valmob['o_price']?>" />
                                            <input type="hidden" name="currency_code" value="Rs" />
                                            <input type="hidden" name="return" value=" <?php echo $valmob['id']?>" />
                                            <input type="hidden" name="shipping" value="mob_<?php echo $valmob['id']?>" />
                                            
                                            <input type="hidden" name="cancel_return" value=" " />
                                            <input type="submit" name="submit" value="Add to cart" class="button" />
                                        </fieldset>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>


                        <?php
                    }
                    ?>
                </div>
                    <!---------------------------  Loop Starts Here  -------------------------->

                   
                    <!------------------------  Loop Ends Here ---------------------->

                <!----------------------------------  End Of Cate Gory Wise Tab ------------------------------->
                <!--//tab_one-->

                <!--/tab_two-->
                <div class="tab2">
<?php
foreach( $rowheadphone as $valmob){
                        ?>

                             <div class="col-md-3 product-men">
                        <div class="men-pro-item simpleCart_shelfItem">
                            <div class="men-thumb-item">
                                <img src="<?php echo $valmob['img1']; ?>" alt="" class="pro-image-front" style="height: 350px">
                                <img src="<?php echo $valmob['img2']; ?>" alt="" class="pro-image-back" style="height: 350px">
                                <div class="men-cart-pro">
                                    <div class="inner-men-cart-pro">
                                        <a href="#" class="link-product-add-cart other-product" id="<?php echo $valmob['id']?>">Quick View</a>
                                    </div>
                                </div>
                                <span class="product-new-top">New</span>

                            </div>
                            <div class="item-info-product ">
                                <h4 style="color:#f00;"><?php echo $valmob['name']; ?></h4>
                                <div class="info-product-price">
                                    <?php

                                    ?>
                                    <span class="item_price"><?php echo $valmob['o_price']!=''? $valmob['o_price']."/- Rs":$valmob['c_price']."/- Rs" ?></span>
                                    <del><?php echo $valmob['o_price']!=''? $valmob['c_price']."/- Rs":false?></del>
                                </div>
                                <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
                                    <form action="#" method="post">
                                        <fieldset>
                                            <input type="hidden" name="cmd" value="_cart" />
                                            <input type="hidden" name="add" value="1" />
                                            <input type="hidden" name="business" value="<?php echo $valmob['id']?>" />
                                            <input type="hidden" name="item_name" value="<?php echo $valmob['name']?>" />
                                            <input type="hidden" name="amount" value="<?php  echo $valmob['o_price']==''? $valmob['c_price']:$valmob['o_price']?>" />
                                            <input type="hidden" name="discount_amount" value="<?php //echo $valmob['o_price']?>" />
                                            <input type="hidden" name="currency_code" value="Rs" />
                                             <input type="hidden" name="shipping" value="other_<?php echo $valmob['id']?>" />
                                            
                                            <input type="hidden" name="return" value=" " />
                                            <input type="hidden" name="cancel_return" value=" " />
                                            <input type="submit" name="submit" value="Add to cart" class="button" />
                                        </fieldset>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>


                        <?php
                    }
                    ?>
                    </div>
                <!--//tab_two-->


                <div class="tab3">
                <?php
foreach( $rowbattery as $valmob){
                        ?>

                             <div class="col-md-3 product-men">
                        <div class="men-pro-item simpleCart_shelfItem">
                            <div class="men-thumb-item">
                                <img src="<?php echo $valmob['img1']; ?>" alt="" class="pro-image-front" style="height: 350px">
                                <img src="<?php echo $valmob['img2']; ?>" alt="" class="pro-image-back" style="height: 350px">
                                <div class="men-cart-pro">
                                    <div class="inner-men-cart-pro">
                                        <a href="#" class="link-product-add-cart other-product" id="<?php echo $valmob['id']?>" >Quick View</a>
                                    </div>
                                </div>
                                <span class="product-new-top">New</span>

                            </div>
                            <div class="item-info-product ">
                                <h4 style="color:#f00;"><?php echo $valmob['name']; ?></h4>
                                <div class="info-product-price">
                                    <?php

                                    ?>
                                    <span class="item_price"><?php echo $valmob['o_price']!=''? $valmob['o_price']."/- Rs":$valmob['c_price']."/- Rs" ?></span>
                                    <del><?php echo $valmob['o_price']!=''? $valmob['c_price']."/- Rs":false?></del>
                                </div>
                                <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
                                    <form action="#" method="post">
                                        <fieldset>
                                            <input type="hidden" name="cmd" value="_cart" />
                                            <input type="hidden" name="add" value="1" />
                                            <input type="hidden" name="business" value="<?php echo $valmob['id']?>" />
                                            <input type="hidden" name="item_name" value="<?php echo $valmob['name']?>" />
                                            <input type="hidden" name="amount" value="<?php echo $valmob['o_price']==''? $valmob['c_price']:$valmob['o_price']?>" />
                                            <input type="hidden" name="discount_amount" value="<?php //echo $valmob['o_price']?>" />
                                            <input type="hidden" name="currency_code" value="Rs" />
                                             <input type="hidden" name="shipping" value="other_<?php echo $valmob['id']?>" />
                                            
                                            <input type="hidden" name="return" value=" " />
                                            <input type="hidden" name="cancel_return" value=" " />
                                            <input type="submit" name="submit" value="Add to cart" class="button" />
                                        </fieldset>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>


                        <?php
                    }
                    ?>
                   
                </div>
                <div class="tab4">
<?php
foreach( $rowaccessories as $valmob){
                        ?>

                             <div class="col-md-3 product-men">
                        <div class="men-pro-item simpleCart_shelfItem">
                            <div class="men-thumb-item">
                                <img src="<?php echo $valmob['img1']; ?>" alt="" class="pro-image-front" style="height: 350px">
                                <img src="<?php echo $valmob['img2']; ?>" alt="" class="pro-image-back" style="height: 350px">
                                <div class="men-cart-pro">
                                    <div class="inner-men-cart-pro">
                                        <a href="#" class="link-product-add-cart other-product" id="<?php echo $valmob['id']?>" >Quick View</a>
                                    </div>
                                </div>
                                <span class="product-new-top">New</span>

                            </div>
                            <div class="item-info-product ">
                                <h4 style="color:#f00;"><?php echo $valmob['name']; ?></h4>
                                <div class="info-product-price">
                                    <?php

                                    ?>
                                    <span class="item_price"><?php echo $valmob['o_price']!=''? $valmob['o_price']."/- Rs":$valmob['c_price']."/- Rs" ?></span>
                                    <del><?php echo $valmob['o_price']!=''? $valmob['c_price']."/- Rs":false?></del>
                                </div>
                                <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
                                    <form action="#" method="post">
                                        <fieldset>
                                            <input type="hidden" name="cmd" value="_cart" />
                                            <input type="hidden" name="add" value="1" />
                                            <input type="hidden" name="business" value="<?php echo $valmob['id']?>" />
                                            <input type="hidden" name="item_name" value="<?php echo $valmob['name']?>" />
                                            <input type="hidden" name="amount" value="<?php echo $valmob['o_price']==''? $valmob['c_price']:$valmob['o_price']?>" />
                                            <input type="hidden" name="discount_amount" value="<?php // echo $valmob['o_price']?>" />
                                            <input type="hidden" name="currency_code" value="Rs" />
                                             <input type="hidden" name="shipping" value="other_<?php echo $valmob['id']?>" />
                                            
                                            <input type="hidden" name="return" value=" " />
                                            <input type="hidden" name="cancel_return" value=" " />
                                            <input type="submit" name="submit" value="Add to cart" class="button" />
                                        </fieldset>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>


                        <?php
                    }
                    ?>

                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //new_arrivals -->