<?php require_once 'mallick_admincp/lib/$_config.php'; session_start(); ?>
<!DOCTYPE html>
<html>
<head>
<title>Welcome To Mallick Mobile</title>
<!--/tags -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Elite Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //tags -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<link href="css/font-awesome.css" rel="stylesheet"> 
<link href="css/easy-responsive-tabs.css" rel='stylesheet' type='text/css'/>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />

<!-- //for bootstrap working -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,900italic,700italic' rel='stylesheet' type='text/css'>
</head>
<body>
<!-- header -->
<?php require_once 'lib/_header.php'; ?>
<!-- Modal1 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
						<div class="modal-body modal-body-sub_agile">
						<div class="col-md-8 modal_body_left modal_body_left1">
						<h3 class="agileinfo_sign">Sign In <span>Now</span></h3>
									<form action="#" method="post">
							<div class="styled-input agile-styled-input-top">
								<input type="text" name="Name" required="">
								<label>Name</label>
								<span></span>
							</div>
							<div class="styled-input">
								<input type="email" name="Email" required=""> 
								<label>Email</label>
								<span></span>
							</div> 
							<input type="submit" value="Sign In">
						</form>
						  <ul class="social-nav model-3d-0 footer-social w3_agile_social top_agile_third">
															<li><a href="#" class="facebook">
																  <div class="front"><i class="fa fa-facebook" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="twitter"> 
																  <div class="front"><i class="fa fa-twitter" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="instagram">
																  <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-instagram" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="pinterest">
																  <div class="front"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-linkedin" aria-hidden="true"></i></div></a></li>
														</ul>
														<div class="clearfix"></div>
														<p><a href="#" data-toggle="modal" data-target="#myModal2" > Don't have an account?</a></p>

						</div>
						<div class="col-md-4 modal_body_right modal_body_right1">
							<img src="images/log_pic.jpg" alt=" "/>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- //Modal content-->
			</div>
		</div>
<!-- //Modal1 -->
<!-- Modal2 -->
		<div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
						<div class="modal-body modal-body-sub_agile">
						<div class="col-md-8 modal_body_left modal_body_left1">
						<h3 class="agileinfo_sign">Sign Up <span>Now</span></h3>
									<form action="#" method="post">
							<div class="styled-input agile-styled-input-top">
								<input type="text" name="Name" required="">
								<label>Name</label>
								<span></span>
							</div>
							<div class="styled-input">
								<input type="email" name="Email" required=""> 
								<label>Email</label>
								<span></span>
							</div> 
							<div class="styled-input">
								<input type="password" name="password" required=""> 
								<label>Password</label>
								<span></span>
							</div> 
							<div class="styled-input">
								<input type="password" name="Confirm Password" required=""> 
								<label>Confirm Password</label>
								<span></span>
							</div> 
							<input type="submit" value="Sign Up">
						</form>
						  <ul class="social-nav model-3d-0 footer-social w3_agile_social top_agile_third">
															<li><a href="#" class="facebook">
																  <div class="front"><i class="fa fa-facebook" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="twitter"> 
																  <div class="front"><i class="fa fa-twitter" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="instagram">
																  <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-instagram" aria-hidden="true"></i></div></a></li>
															<li><a href="#" class="pinterest">
																  <div class="front"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
																  <div class="back"><i class="fa fa-linkedin" aria-hidden="true"></i></div></a></li>
														</ul>
														<div class="clearfix"></div>
														<p><a href="#">By clicking register, I agree to your terms</a></p>

						</div>
						<div class="col-md-4 modal_body_right modal_body_right1">
							<img src="images/log_pic.jpg" alt=" "/>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- //Modal content-->
			</div>
		</div>
<!-- //Modal2 -->
<!--/single_page-->
       <!-- /banner_bottom_agile_info -->
<?php 
    $stmt = $link->prepare("SELECT `id`, `img1`, `img2`, `img3`, `name`, `c_price`, `o_price`, `rate`, `des`, `info`, `charge`, `p_id` FROM `all_other_item` WHERE `id` = ?");
	$stmt->bind_param('i', $_SESSION['other_single']);
	$stmt->execute();
	$reslt = $stmt->get_result();
	$mobile = $reslt->fetch_assoc();
	
	$sql = $link->query("SELECT `name` FROM `all_other_product` WHERE `id`= ".$mobile['p_id']."");
	$row = @mysqli_fetch_assoc($sql);
?>
<div class="page-head_agile_info_w3l">
		<div class="container">
			<h3><?php echo $row['name']; ?></h3>
	</div>
</div>

  <!-- banner-bootom-w3-agileits -->

<div class="banner-bootom-w3-agileits">
	<div class="container">
	<!---728x90--->
	     <div class="col-md-4 single-right-left ">
			<div class="grid images_3_of_2">
				<div class="flexslider">
					
					<ul class="slides">
						<li data-thumb="<?php echo $mobile['img1']; ?>">
							<div class="thumb-image"> <img src="<?php echo $mobile['img1']; ?>" data-imagezoom="true" class="img-responsive"> </div>
						</li>
						<li data-thumb="<?php echo $mobile['img2']; ?>">
							<div class="thumb-image"> <img src="<?php echo $mobile['img2']; ?>" data-imagezoom="true" class="img-responsive"> </div>
						</li>	
						<li data-thumb="<?php echo $mobile['img3']; ?>">
							<div class="thumb-image"> <img src="<?php echo $mobile['img3']; ?>" data-imagezoom="true" class="img-responsive"> </div>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>	
			</div>
		</div>
		<div class="col-md-8 single-right-left simpleCart_shelfItem">
					<h3><?php echo $row['name']." ".$mobile['name']; ?></h3>
					<p><?php if(!empty($mobile['o_price'])){ ?>
					<span class="item_price"><i class="fa fa-rupee"></i> <?php echo $mobile['o_price']; ?></span> <del>- <i class="fa fa-rupee"></i> <?php echo $mobile['c_price']; ?></del><?php }else { ?>
					<span class="item_price"><i class="fa fa-rupee"></i> <?php echo $mobile['c_price']; ?></span> <?php }?>
						</p>
					<div class="rating1">
						<span class="starRating">
							<input id="rating5" type="radio" name="rating" value="5" <?php if($mobile['rate'] == 5) echo "checked=\"\""; ?> />
							<label for="rating5">5</label>
							<input id="rating4" type="radio" name="rating" value="4" <?php if($mobile['rate'] == 4) echo "checked=\"\""; ?> />
							<label for="rating4">4</label>
							<input id="rating3" type="radio" name="rating" value="3" <?php if($mobile['rate'] == 3) echo "checked=\"\""; ?> />
							<label for="rating3">3</label>
							<input id="rating2" type="radio" name="rating" value="2" <?php if($mobile['rate'] == 2) echo "checked=\"\""; ?> />
							<label for="rating2">2</label>
							<input id="rating1" type="radio" name="rating" value="1" <?php if($mobile['rate'] == 1) echo "checked=\"\""; ?> />
							<label for="rating1">1</label>
						</span>
					</div>
					
					<div class="description">
						<h5>Check delivery, payment options and charges at your location</h5>
						<input type="text" placeholder="Enter pincode" id="txt_val" maxlength="6" required />
						<input type="submit" value="Check" id="check_pin" /><br />
                        <span id="print_pin" style="font-size: 14px; color: #0eb51bfc; font-family: cursive; font-weight: 600;"></span>
					</div>
					
					
					<div class="color-quality">
						<div class="color-quality-right" style="margin-top: 80px;">
							<h5>Quality :</h5>
							<select id="country1" onchange="change_country(this.value)" class="frm-field required sect">
								<option value="1 Qty">1 Qty</option>
								<option value="2 Qty">2 Qty</option> 
								<option value="3 Qty">3 Qty</option>												
							</select>
						</div>
					</div><br /><br />
					<!--<div class="occasional">
						<h5>Types :</h5>
						<div class="colr ert">
							<label class="radio"><input type="radio" name="radio" checked=""><i></i>Casual Shoes</label>
						</div>
						<div class="colr">
							<label class="radio"><input type="radio" name="radio"><i></i>Sneakers </label>
						</div>
						<div class="colr">
							<label class="radio"><input type="radio" name="radio"><i></i>Formal Shoes</label>
						</div>
						<div class="clearfix"> </div>
					</div>-->
					<div class="occasion-cart">
						<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
															<form action="#" method="post">
																<fieldset>
																	<input type="submit" name="submit" value="Book Now" class="button">
																</fieldset>
															</form>
														</div>
																			
					</div>
				<!-- <ul class="social-nav model-3d-0 footer-social w3_agile_social single_page_w3ls">
					<li class="share">Share On : </li>
					<li><a href="#" class="facebook">
						  <div class="front"><i class="fa fa-facebook" aria-hidden="true"></i></div>
						  <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div></a></li>
					<li><a href="#" class="twitter"> 
						  <div class="front"><i class="fa fa-twitter" aria-hidden="true"></i></div>
						  <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i></div></a></li>
					<li><a href="#" class="instagram">
						  <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i></div>
						  <div class="back"><i class="fa fa-instagram" aria-hidden="true"></i></div></a></li>
					<li><a href="#" class="pinterest">
						  <div class="front"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
						  <div class="back"><i class="fa fa-linkedin" aria-hidden="true"></i></div></a></li>
				</ul> -->
					
		      </div>
	 			<div class="clearfix"> </div>
				<!---728x90--->
				<!-- /new_arrivals --> 
	<div class="responsive_tabs_agileits"> 
				<div id="horizontalTab">
						<ul class="resp-tabs-list">
							<li>Description</li>
							<li>Reviews</li>
							<li>Information</li>
						</ul>
					<div class="resp-tabs-container">
					<!--/tab_one-->
					   <div class="tab1">

							<div class="single_page_agile_its_w3ls">
							<?php echo $mobile['des']; ?>
							</div>
						</div>
						<!--//tab_one-->
						<div class="tab2">
							
							<div class="single_page_agile_its_w3ls">
								<div class="bootstrap-tab-text-grids">
									<div class="bootstrap-tab-text-grid">
										<div class="bootstrap-tab-text-grid-left">
											<img src="images/t1.jpg" alt=" " class="img-responsive">
										</div>
										<div class="bootstrap-tab-text-grid-right">
											<ul>
												<li><a href="#">Admin</a></li>
												
											</ul>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elPellentesque vehicula augue eget.Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis 
												suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem 
												vel eum iure reprehenderit.</p>
										</div>
										<div class="clearfix"> </div>
						             </div>
									 <div class="add-review">
										<h4>add a review</h4>
										<form action="#" method="post">
												<input type="text" name="name" Placeholder="Name" required />
												<input type="email" name="email" Placeholder="Email" required /> 
												<textarea name="msg" Placeholder="Message" required></textarea>
											<input type="submit" value="SEND">
										</form>
									</div>
								 </div>
								 
							 </div>
						 </div>
						   <div class="tab3">

							<div class="single_page_agile_its_w3ls">
							<?php echo $mobile['info']; ?>
							</div>
						</div>
					</div>
				</div>	
			</div>
	<!-- //new_arrivals --> 
	  	<!--/slider_owl-->
	
			<div class="w3_agile_latest_arrivals">
			<h3 class="wthree_text_info">Related <span>Product</span></h3>
			<?php 
		    $stmt = $link->prepare("SELECT `id`, `img1`, `img2`, `name`, `c_price`, `o_price`, `out_stk`, `new_id` FROM `all_other_item` WHERE `p_id` = ? LIMIT 0, 4");
		    $stmt->bind_param('i', $_SESSION['other_id']);
			$stmt->execute();
			$result = $stmt->get_result();
	        while($mobiles = $result->fetch_assoc()){
				if($mobiles['out_stk'] == 1){
		?>
		
			    <div class="col-md-3 product-men">
					<div class="men-pro-item simpleCart_shelfItem">
						<div class="men-thumb-item">
							<img src="<?php echo $mobiles['img1']; ?>" alt="" class="pro-image-front" height="255px" width="180px">
							<img src="<?php echo $mobiles['img2']; ?>" alt="" class="pro-image-back" height="255px" width="180px">
							<div class="men-cart-pro">
								<!--<div class="inner-men-cart-pro">
									<a href="#" class="link-product-add-cart" id="<?php echo $mobile['id']; ?>">Quick View</a>
								</div>-->
							</div>
							<?php if($mobiles['out_stk'] == 1){
								echo "<span class=\"product-new-top\">Out Of Stock</span>";
							} ?>
							
						</div>
						<div class="item-info-product">
							<h4><a href="#"><?php echo $mobiles['name']; ?></a></h4>
							<div class="info-product-price">
							<?php if(!empty($mobiles['o_price'])){ ?>
								<span class="item_price"><i class="fa fa-rupee"></i> <?php echo $mobiles['o_price']; ?></span>
								<del><i class="fa fa-rupee"></i> <?php echo $mobiles['c_price']; ?></del>
							<?php }else{ ?> 
							    <span class="item_price"><i class="fa fa-rupee"></i> <?php echo $mobiles['c_price']; ?></span>
							<?php } ?>
							</div>
							<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
								<form action="#" method="post">
									<fieldset>
										<a href="#" class="add-carts" id="<?php echo $mobiles['id']; ?>"><input type="button" name="submit" value="Book Now" class="button" /></a>
									</fieldset>
								</form>
							</div>
						</div><!-- item-info-product -->
					</div><!-- men-pro-item simpleCart_shelfItem -->
				</div><!-- col-md-3 product-men -->
				
				
				<?php }else{ 
				?>
				
				
				<div class="col-md-3 product-men">
					<div class="men-pro-item simpleCart_shelfItem">
						<div class="men-thumb-item">
							<img src="<?php echo $mobiles['img1']; ?>" alt="" class="pro-image-front" height="255px" width="180px">
							<img src="<?php echo $mobiles['img2']; ?>" alt="" class="pro-image-back" height="255px" width="180px">
							<div class="men-cart-pro">
								<div class="inner-men-cart-pro">
									<a href="#" class="link-product-add-cart other-product" id="<?php echo $mobiles['id']; ?>">Quick View</a>
								</div>
							</div>
							<?php if($mobiles['new_id'] == 1){
								echo "<span class=\"product-new-top\">New</span>";
							} ?>
							
						</div>
						<div class="item-info-product">
							<h4><a href="#"><?php echo $mobiles['name']; ?></a></h4>
							<div class="info-product-price">
							<?php if(!empty($mobiles['o_price'])){ ?>
								<span class="item_price"><i class="fa fa-rupee"></i> <?php echo $mobiles['o_price']; ?></span>
								<del><i class="fa fa-rupee"></i> <?php echo $mobiles['c_price']; ?></del>
							<?php }else{ ?> 
							    <span class="item_price"><i class="fa fa-rupee"></i> <?php echo $mobiles['c_price']; ?></span>
							<?php } ?>
							</div>
							<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
								<form action="#" method="post">
									<fieldset>
										<a href="#" class="add-cart other-products" id="<?php echo $mobiles['id']; ?>"><input type="button" name="submit" value="Book Now" class="button" /></a>
									</fieldset>
								</form>
							</div>
						</div><!-- item-info-product -->
					</div><!-- men-pro-item simpleCart_shelfItem -->
				</div><!-- col-md-3 product-men -->
				<?php
				} ?>			
		<?php 
			}
		?>
							<div class="clearfix"> </div>
					<!--//slider_owl-->
		         </div>
	        </div>
 </div>
<!--//single_page-->
<!---728x90--->
<!--/grids-->
<?php require_once 'lib/_grids.php'; ?>
<!--grids-->
<!-- footer -->
<?php require_once 'lib/_footer.php'; ?>
<!-- //footer -->

<!-- login -->
			<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content modal-info">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						</div>
						<div class="modal-body modal-spa">
							<div class="login-grids">
								<div class="login">
									<div class="login-bottom">
										<h3>Sign up for free</h3>
										<form>
											<div class="sign-up">
												<h4>Email :</h4>
												<input type="text" value="Type here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">	
											</div>
											<div class="sign-up">
												<h4>Password :</h4>
												<input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
												
											</div>
											<div class="sign-up">
												<h4>Re-type Password :</h4>
												<input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
												
											</div>
											<div class="sign-up">
												<input type="submit" value="REGISTER NOW" >
											</div>
											
										</form>
									</div>
									<div class="login-right">
										<h3>Sign in with your account</h3>
										<form>
											<div class="sign-in">
												<h4>Email :</h4>
												<input type="text" value="Type here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">	
											</div>
											<div class="sign-in">
												<h4>Password :</h4>
												<input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
												<a href="#">Forgot password?</a>
											</div>
											<div class="single-bottom">
												<input type="checkbox"  id="brand" value="">
												<label for="brand"><span></span>Remember Me.</label>
											</div>
											<div class="sign-in">
												<input type="submit" value="SIGNIN" >
											</div>
										</form>
									</div>
									<div class="clearfix"></div>
								</div>
								<p>By logging in you agree to our <a href="#">Terms and Conditions</a> and <a href="#">Privacy Policy</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
<!-- //login -->
<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<script src="js/modernizr.custom.js"></script>
	<!-- Custom-JavaScript-File-Links --> 
	<!-- cart-js -->
	<script src="js/minicart.min.js"></script>
	<script type="text/javascript">
	jQuery(document).ready(function($) {
		$("#check_pin").click(function(event){
			var txt_val = $("#txt_val").val();
			var pin = '<?php echo $pin; ?>';
			var charges = '<?php echo $mobile['charge']; ?>';
			if(txt_val == ""){
				alert("Please Enter Pin Code")
			}else{
				if(pin == txt_val){		
					$("#print_pin").html("Delivery Charge Free");
				}else{
					var text1 = '<i class="fa fa-rupee"></i> '+charges+' Delivery Charge Extra';
					$("#print_pin").html(text1);
				}
			}
		});
	});
</script>
<script>
	// Mini Cart
	paypal.minicart.render({
		action: '#'
	});

	if (~window.location.search.indexOf('reset=true')) {
		paypal.minicart.reset();
	}
</script>

	<!-- //cart-js --> 
	<!-- single -->
<script src="js/imagezoom.js"></script>
<!-- single -->
<!-- script for responsive tabs -->						
<script src="js/easy-responsive-tabs.js"></script>
<script>
	$(document).ready(function () {
	$('#horizontalTab').easyResponsiveTabs({
	type: 'default', //Types: default, vertical, accordion           
	width: 'auto', //auto or any width like 600px
	fit: true,   // 100% fit in a container
	closed: 'accordion', // Start closed if in accordion view
	activate: function(event) { // Callback function if tab is switched
	var $tab = $(this);
	var $info = $('#tabInfo');
	var $name = $('span', $info);
	$name.text($tab.text());
	$info.show();
	}
	});
	$('#verticalTab').easyResponsiveTabs({
	type: 'vertical',
	width: 'auto',
	fit: true
	});
	});
</script>
<!-- FlexSlider -->
<script src="js/jquery.flexslider.js"></script>
						<script>
						// Can also be used with $(document).ready()
							$(window).load(function() {
								$('.flexslider').flexslider({
								animation: "slide",
								controlNav: "thumbnails"
								});
							});
						</script>
					<!-- //FlexSlider-->
<!-- //script for responsive tabs -->		
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/jquery.easing.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->

<!-- for bootstrap working -->
<script type="text/javascript" src="js/bootstrap.js"></script>
<?php require_once 'lib/_all_list.php'; ?>
</body>

<!-- Mirrored from p.w3layouts.com/demos_new/template_demo/20-06-2017/elite_shoppy-demo_Free/143933984/web/single.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 28 Oct 2017 12:27:15 GMT -->
</html>
