<?php
    require_once 'lib/$_functions.php'; 
	sec_session_start();
	if (login_check($link) == true)
	{
		require_once 'lib/_reduse.php'; 
		extract($_POST);
		if(isset($insert)){
		  $valid_exts = array('jpeg', 'jpg', 'JPG', 'JPEG');
		  for($i=0; $i < 3; $i++){
		  	$ext = strtolower(pathinfo($_FILES['image']['name'][$i], PATHINFO_EXTENSION));
		  	$path     = '../all_img/'.rand(1, 9999).'_'.time().'.'.$ext;		// File store in image folder
            $img_name = compress_image($_FILES["image"]["tmp_name"][$i], $path, 50); // Compress File in KB, (Here 10 is a percentege size of total size orginal file)
		    $img_path = explode("../", $img_name);
		    $imgPath[] = $img_path[1];
		  }
		  
		 /* $name = htmlentities(trim(addslashes(strip_tags($name))));
		  $c_price = htmlentities(trim(addslashes(strip_tags($c_price))));
		  $o_price = htmlentities(trim(addslashes(strip_tags($o_price))));
		  $charge = htmlentites(trim(addslashes(strip_tags($charge))));
		  */
		  $stmt = $link->prepare("INSERT INTO `all_other_item`(`img1`, `img2`, `img3`, `name`, `c_price`, `o_price`, `rate`, `des`, `info`, `charge`, `out_stk`, `p_id`,`category`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
		  $stmt->bind_param('sssssssssssss', $imgPath[0], $imgPath[1], $imgPath[2], $name, $c_price, $o_price, $rate, $des, $info, $charge, $out_stk, $p_id,$category);
		  if($stmt->execute()){
				echo "<script type=\"text/javascript\">
                        alert('Successfully Insert');
                        window.location='all_other_product' 
                      </script>";
			}else{
				echo "<script type=\"text/javascript\">
                        alert('Unsuccessfully Insert');
                        window.location='all_other_product' 
                      </script>";
			}
		}
		
		if(isset($_GET['acti']) && isset($_GET['uid'])){
			
			$link->query("UPDATE `all_other_item` SET `new_id`=".$_GET['acti']." WHERE `id` = ".$_GET['uid']."");
			echo "<script type=\"text/javascript\">
                        alert('Successfully Set');
                        window.location='all_other_product' 
                      </script>";
		}
		
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <?php require_once 'lib/$_title.php'; ?>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
    <link rel="stylesheet" href="js/mycss.css" />
	
	<!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css" />
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>-->
<link rel="stylesheet" href="css/stylesheet-image-based.css" />
<script src="//cdn.ckeditor.com/4.7.3/basic/ckeditor.js"></script>
  </head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

   <?php require_once 'lib/$_header.php'; ?>
  <!-- Left side column. contains the logo and sidebar -->
   <?php require_once 'lib/$_menu.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Mobile List 
        <small>Mobile Product</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-pencil"></i> All Mobile List</a></li>
        <li class="active">Mobile Product</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

        <div class="row">
			<div class="col-md-12">
			    <div class="box">
				    <div class="box-header with-border">
				        <h3 class="box-title"><a href="all_other_product?action=<?php echo md5('insert'); ?>"><i class="fa fa-pencil"></i> Add New</a></h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div><!-- /.box-header -->
						
					<div class="box-body">
					<?php 
					   if($_GET['action'] == md5('insert')){
					?>
					     <form method="post" action="all_other_product" enctype="multipart/form-data">
					     
					     
					   
							   <div class="col-md-6">
			                    <label>Product Type :</label>
			                    <input name="category" list="prdct_cat" class="form-control"  style="width: 100%;" required>
			                    <datalist id="prdct_cat"  >
			                      
			                      <?php 
			                          $cat = $link->prepare("SELECT `category` FROM `all_other_item` GROUP BY `category` ORDER BY `category` ASC");
			                        $cat->execute();
			                          $result2 = $cat->get_result();
						              while($data2 = $result2->fetch_assoc()){
						          ?>
						                <option value="<?php echo $data2['category']; ?>"><?php echo $data2['category']; ?></option>
						          <?php    	
						              }
			                      ?>
			                    </datalist>
			                  </div>
			              </div>



					   
					   
							   <div class="col-md-6">
			                    <label>Product Brand :</label>
			                    <select class="form-control select2" name="p_id" style="width: 100%;" required>
			                      <option value="" selected="selected">Please Select Brand</option>
			                      <?php 
			                          $mobile_brand = $link->prepare("SELECT * FROM `all_other_product` ORDER BY `name` ASC");
			                          $mobile_brand->execute();
			                          $result = $mobile_brand->get_result();
						              while($data = $result->fetch_assoc()){
						          ?>
						                <option value="<?php echo $data['id']; ?>"><?php echo $data['name']; ?></option>
						          <?php    	
						              }
			                      ?>
			                    </select>
			                  </div><!-- /.col-md-6 -->
			                  
			                  <div class="col-md-6">
			                    <label>Product Model Name :</label>
			                    <div class="input-group">
			                      <div class="input-group-addon">
			                        <i class="fa fa-mobile"></i>
			                      </div>
			                      <input type="text" class="form-control" name="name" placeholder="Product Model Name" required />
			                    </div><!-- /.input group -->
			                  </div><!-- /.form group -->
			                  
			                  <div class="col-md-6">
			                    <label>Primary Price :</label>
			                    <div class="input-group">
			                      <div class="input-group-addon">
			                        <i class="fa fa-rupee"></i>
			                      </div>
			                      <input type="text" class="form-control" name="c_price" placeholder="Primary Price" required />
			                    </div><!-- /.input group -->
			                  </div><!-- /.form group -->
			                  
			                  <div class="col-md-6">
			                    <label>Offer Price (Optional) :</label>
			                    <div class="input-group">
			                      <div class="input-group-addon">
			                        <i class="fa fa-rupee"></i>
			                      </div>
			                      <input type="text" class="form-control" name="o_price" placeholder="Offer Price" />
			                    </div><!-- /.input group -->
			                  </div><!-- /.form group -->
			                  
			                  
			             
						 
						    <div class="col-md-6">
			                    <label>Product Rating :</label>
			                    <select class="form-control select2" name="rate" style="width: 100%;" required>
			                      <option value="" selected="selected">Please Product Rating</option>
			                      <option value="1">1 Star</option>
			                      <option value="2">2 Star</option>
			                      <option value="3">3 Star</option>
			                      <option value="4">4 Star</option>
			                      <option value="5">5 Star</option>
			                    </select>
			                  </div><!-- /.col-md-6 -->
			                  
			                  <div class="col-md-6">
			                    <label>Delivery Charges :</label>
			                    <div class="input-group">
			                      <div class="input-group-addon">
			                        <i class="fa fa-rupee"></i>
			                      </div>
			                      <input type="text" class="form-control" name="charge" placeholder="Delivery Charges" required />
			                    </div><!-- /.input group -->
			                  </div><!-- /.form group -->
			                  
			                  <div class="col-md-6">
			                    <label>Out Of Stock :</label>
			                    <div class="input-group">
			                    <div class="example" style="height: 11px;">
			                      <div>
                                  <input id="radio1" type="radio" name="out_stk" value="0" checked="checked"><label for="radio1"><span><span></span></span>No</label>
                                 </div>
                                 <div style="float: left; margin-left: 60px; margin-top: -28px;">
                                  <input id="radio2" type="radio" name="out_stk" value="1"><label for="radio2"><span><span></span></span>Yes</label>
                                </div>
			                      </div>
			                    </div><!-- /.input group -->
			                  </div><!-- /.form group -->
			                  
			                  <div class="col-md-6">
			                    <label>Product Image (Only 3 Images) :</label>
			                    <div class="input-group">
			                      <div class="input-group-addon">
			                        <i class="fa fa-image"></i>
			                      </div>
			                      <input type="file" class="form-control" name="image[]" multiple required />
			                    </div><!-- /.input group -->
			                  </div><!-- /.form group -->
			                  
			                  
						 
						 <div class="col-md-12">
						     <div class="col-md-6">
			                    <label>Description :</label>
			                   
			                      <textarea class="form-control" id="editor1" name="des"></textarea>
			                      <script>CKEDITOR.replace( 'editor1' );</script>
			                    
			                  </div><!-- /.form group -->
			                  
			                  <div class="col-md-6">
			                    <label>Information :</label>
			                   
			                      <textarea class="form-control" id="editor2" name="info"></textarea>
			                      <script>CKEDITOR.replace( 'editor2' );</script>
			                    
			                  </div><!-- /.form group -->
			                  <div class="clear"></div><br>
			                  <button type="submit" class="btn btn-primary" name="insert" style="margin: 30px;"><i class="fa fa-fw fa-save"></i> Save</button>
						 </div><!-- col-md-12 -->
						 
					     </form>
					<?php   
					   }
					?>
                    <table class="table table-bordered table-striped" id="example1">
                    <thead>
					    <tr>
						    <th>#</th>
						    <th>Product Image</th>
						    <th>Product Name</th>
						    <th>Out Of Stock</th>
						    <th>Brand Name</th>
						    <th>Category</th>
						    <th>Current Price</th>
						    <th>Offer Price</th>
						    <th>New Product</th>
						    <th>Action</th>
						</tr>
                    </thead>
                    <tbody>
					<?php 
						$cont = 1;
					    $stmt = $link->prepare("SELECT * FROM `all_other_item` ORDER BY `id` DESC");
						$stmt->execute();
						$result = $stmt->get_result();
						while($data = $result->fetch_assoc()){
							
					?>
						<tr>
							<td><?php echo $cont; ?></td>
							<td><img src="../<?php echo $data['img1']; ?>" height="100px" width="100px" /></td>
							<td><?php echo $data['name']; ?></td>
							<td><?php if($data['out_stk'] == 0) echo "No"; else echo "Yes"; ?></td>
							<td>
							<?php $sql = $link->query("SELECT `name` FROM `all_other_product` WHERE `id` = ".$data['p_id'].""); $row = @mysqli_fetch_assoc($sql); echo $row['name']; ?>
							</td>
							<td><?php echo $data['category'] ?></td>
							<td><?php echo $data['c_price'] ?></td>
							<td><?php echo $data['o_price'] ?></td>
							<td>
							<?php if($data['new_id'] == 0){
							?>
							<a href="all_other_product?acti=1&uid=<?php echo $data['id']; ?>"><i class="fa fa-toggle-off"></i> Old Product</a>
							<?php 
							}else{
							?>
							<a href="all_other_product?acti=0&uid=<?php echo $data['id']; ?>"><i class="fa fa-toggle-on"></i> New Product</a>
							<?php 
							}
							?>
							</td>
							<td>
								<a href="edit_other_product_list?edit=<?php echo $data['id']; ?>"><i class="fa fa-pencil"></i> Edit</a> &nbsp;&nbsp;&nbsp;&nbsp;
							    <a href="#" id="<?php echo $data['id']; ?>" class="delete" title="Delete"><i class="fa fa-trash"></i> Delete</a>
							</td>
						</tr>
                    <?php					
							
					    $cont++;
						}
					?>
                    </tbody>
					
                    <!--<tfoot>
                      <tr>
                        <th>Rendering engine</th>
                        <th>Browser</th>
                        <th>Platform(s)</th>
                        <th>Engine version</th>
                        <th>CSS grade</th>
                      </tr>
                    </tfoot>-->
                    </table>
					</div><!-- /.box-body -->
					  
			    </div><!-- /.box -->
            </div> <!-- /.col -->
        </div> <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require_once 'lib/$_footer.php'; ?>
</div>
<div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->
    
 <!-- jQuery 2.1.4 -->
 
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
	
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="plugins/select2/select2.full.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	
    <!-- SlimScroll 1.3.0 -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
   <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
      });
    //iCheck for checkbox and radio inputs
      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
      });
    </script>
    <script>
      $(function () {
        $("#example1").DataTable();
      });
    </script>
   <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.1.0/jquery.form.min.js"></script>
	---------------------------------------Insert Data---------------------------------------------------------------------
	<script>
	    function submitform(obj){
			var name = $('#name').val();
			if(name == ''){
				alert("Name is missing");
			}else{
				$(obj).ajaxSubmit({
					success:successForm
				});
			}
			
			return false;
		}
		
		function successForm(result){
		    if(result==1){
				$('.form')[0].reset();
				$('.error').html('<i style=\"color: #05A3D3;\">Successfully Insert</i>');
			}else{
				$('.error').html('<i style=\"color:#d32205;\">Unsuccessfully Insert</i>');
			}
		}
	</script>
	
	<!-----------------------------------------Insert Data--------------------------------------------------------------------->
	<!-----------------------------------------Fetch Data---------------------------------------------------------------------
	<script type="text/javascript">
		$(document).ready(function(){
			setInterval(function(){
				$('#show').load('customer_master_data.php')
			}, 500);
		});
	</script>
	<!-----------------------------------------Fetch Data--------------------------------------------------------------------->
    <!-----------------------------------------Delete Data--------------------------------------------------------------------->
	<script type="text/javascript">
	    $(function(){
			$(".delete").click(function(){
				var element = $(this);
				var userid = element.attr("id");
				var info = 'id=' + userid;
				if(confirm("Are you sure want to delete?")){
					$.ajax({
						url: 'delete_other_product_list.php',
						type: 'post',
						data: info,
						success: function(){
							
						}
					});
					$(this).parent().parent().fadeOut(1500, function(){
						$(this).remove();
					});
				};
				return false;	
			});
		});
	</script>
	
	<!-----------------------------------------Delete Data--------------------------------------------------------------------->
  </body>
</html>
<?php 
	}
	else
	{  
        @mysqli_free_result($LoginData);
		session_destroy();
		header("Location:index");
		
	}
	$link->close();
?>